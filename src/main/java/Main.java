import geometry.Figure;
import geometry.IHandler;
import geometry.Sphere;
import geometry.Cub;

import java.lang.reflect.Constructor;

public class Main {
    public static void main(String[] args) {

        Figure cub = new  Cub(3.6);
        Figure sphere = new Sphere(4.6);
        Constructor constructor = new Constructor();
        constructor.findSquare();

        cub.printFigure();
        sphere.printFigure();
        IHandler[] figures = {(IHandler) cub, (IHandler) sphere, (IHandler) constructor};
        for (IHandler i : figures) {
            System.out.println(i.findSquare());


    }
}
    }

