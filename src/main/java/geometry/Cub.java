package geometry;

public class Cub extends Figure implements IHandler{
    private double height;

    public Cub(){}
    public Cub(double h){
        this.height=h;
    }

    public double getHeight(){
        return height;
    }

    public void setHeight(double h) {
        this.height = h;
    }

    public double findSquare() {
        square = findSideSquare()*6;
        return square;
    }
    public double findSideSquare() {
        return Math.pow(height,2);
    }
}
