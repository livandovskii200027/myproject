package geometry;

public class Sphere extends Figure implements  IHandler{
    private double radius;

    public Sphere(){}
    public Sphere(double height) {
        this.radius = height;
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double h){
        this.radius = h;
    }
    public double findSquare(){
        square = 4*PI*Math.pow(radius, 2);
        return square;
    }
    public void print(){};
}
