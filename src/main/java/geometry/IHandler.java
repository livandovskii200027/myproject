package geometry;

public interface IHandler {
    public double findSquare();
}
